//
//  ViewController.swift
//  Fun Progress Bar
//
//  Created by Jansen Ducusin on 3/3/21.
//

import UIKit

class ViewController: UIViewController {

    var fun:Fun?
    var progressBarFill: CAShapeLayer?
    var downloadStarted = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fun = Fun(view: self.view, radius: 100)
        
        if let fun = self.fun {
            
            self.progressBarFill = fun.createProgressBar()
            
            view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleTap)))
        }
        
    }

    @objc private func handleTap(){
        
        self.downloadFile()
        
    }
    
    private func progressAnimation() {
        if let progressBar = self.progressBarFill {
            let animation = CABasicAnimation(keyPath: "strokeEnd")
            animation.toValue = 1
            animation.duration = 1
            animation.fillMode = .forwards
            animation.isRemovedOnCompletion = false
            
            progressBar.add(animation, forKey: "funAnimation")
        }
    }
    
    private func downloadFile(){
        
        if self.downloadStarted {
            return
        }
        
        self.downloadStarted = true
        
        if let progressBar = self.progressBarFill {
            progressBar.strokeEnd = 0
        }
        
        let urlString = "https://cdnm.meln.top/mr/Simple Plan - Take My Hand.mp3?session_key=182924150387197ec7fd7066e5c52371&hash=2e762a88096097cba51a114bb82e3ee2"
        
        let configuration = URLSessionConfiguration.default
        let operationQueue = OperationQueue()
        let urlSession = URLSession(configuration: configuration, delegate: self, delegateQueue: operationQueue)
     
        guard let escapedUrlString = urlString.addingPercentEncoding(withAllowedCharacters:NSCharacterSet.urlQueryAllowed),
              let url = URL(string: escapedUrlString) else {
            return
        }
        let downloadTask = urlSession.downloadTask(with: url)
        downloadTask.resume()
    }

}

extension ViewController: URLSessionDownloadDelegate{
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        self.downloadStarted = false
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        let percentage = CGFloat(totalBytesWritten) / CGFloat(totalBytesExpectedToWrite)
        
        DispatchQueue.main.async {
            self.progressBarFill?.strokeEnd = percentage
            self.fun?.label.text = "\(Int(percentage * 100))%"
        }
    }
}
