//
//  Fun.swift
//  Fun Progress Bar
//
//  Created by Jansen Ducusin on 3/3/21.
//

import Foundation
import UIKit

class Fun{
    var view: UIView!
    var radius: CGFloat!
    let startAngle: CGFloat = 0
    let endAngle: CGFloat = 2 * CGFloat.pi
    let label = UILabel()
    let lineWidth: CGFloat = 18
    var pulseLayer: CAShapeLayer?
    
    init(view:UIView, radius:CGFloat) {
        self.view = view
        self.radius = radius
        self.setupNotificationObservers()
    }
    
    func createProgressBar() -> CAShapeLayer{
        
        let track = self.drawTrack()
        let fill = self.drawFill()
        let label = self.drawLabel()
        self.pulseLayer = self.drawPulse()
        
        if let view = self.view {
            if let pulse = self.pulseLayer {
                self.animateLayer(pulseLayer: pulse)
                view.layer.addSublayer(pulse)
            }
            
            view.layer.addSublayer(track)
            view.layer.addSublayer(fill)
           
            view.addSubview(label)
            label.frame = CGRect(x:0, y: 0, width: radius, height: radius)
            label.center = view.center
        }
        
        return fill
    }
    
    private func drawLabel() -> UILabel {
        self.label.text = "Start"
        self.label.textAlignment = .center
        self.label.font = UIFont.boldSystemFont(ofSize: 32)
        self.label.textColor = .white
        return self.label
    }
    
    private func drawPulse() -> CAShapeLayer{
        let pulseLayer = CAShapeLayer()
        
        let pulsePath = UIBezierPath(arcCenter: .zero, radius: self.radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
        
        let color = UIColor(red: 0.40, green: 0.00, blue: 0.00, alpha: 1.00).cgColor
        
        pulseLayer.path = pulsePath.cgPath
        pulseLayer.lineWidth = self.lineWidth
        pulseLayer.fillColor = color
        pulseLayer.strokeColor = color
        pulseLayer.position = view.center
        return pulseLayer
    }
    
    private func drawFill() -> CAShapeLayer{
        let fillLayer = CAShapeLayer()
        
        let fillPath = UIBezierPath(arcCenter: .zero, radius: self.radius, startAngle: startAngle, endAngle:endAngle, clockwise: true)
        
        let color = UIColor(red: 1.00, green: 0.00, blue: 0.00, alpha: 1.00).cgColor
        
        fillLayer.path = fillPath.cgPath
        fillLayer.strokeColor = color
        fillLayer.lineWidth = self.lineWidth
        fillLayer.strokeEnd = 0
        fillLayer.lineCap = .round
        fillLayer.fillColor = UIColor.clear.cgColor
        fillLayer.transform = CATransform3DMakeRotation(-CGFloat.pi / 2, 0, 0, 1)
        fillLayer.position = view.center
        
        return fillLayer
    }
    
    private func drawTrack() -> CAShapeLayer{
        let trackPath = UIBezierPath(arcCenter: self.view.center, radius: self.radius, startAngle: startAngle, endAngle:endAngle, clockwise: true)
        
        let color = UIColor(red: 0.25, green: 0.00, blue: 0.00, alpha: 1.00).cgColor
        
        let trackLayer = CAShapeLayer()
        trackLayer.path = trackPath.cgPath
        trackLayer.strokeColor = color
        trackLayer.lineWidth = self.lineWidth
        trackLayer.fillColor = UIColor.black.cgColor
        
        return trackLayer
    }
    
    private func animateLayer(pulseLayer:CAShapeLayer){
        let animation = CABasicAnimation(keyPath: "transform.scale")
        animation.toValue = 1.25
        animation.duration = 0.75
        animation.timingFunction = CAMediaTimingFunction(name: .easeOut)
        animation.autoreverses = true
        animation.repeatCount = .infinity
        pulseLayer.add(animation, forKey: "pulsing")
    }
    
    private func setupNotificationObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleEnterForeground), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc private func handleEnterForeground(){
        if let pulse = self.pulseLayer {
            self.animateLayer(pulseLayer: pulse)
        }
    }
}
